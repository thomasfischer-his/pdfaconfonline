# PDF/A Conformance Checker – Online

This project provides an online interface to validate PDF files for their compliance to PDF/A as assessed by veraPDF and Apache PDFBox.
This is realized by providing a lighttpd configuration and some scripts (Bash and Python) for use as CGI for the web server and local scripts for invoking the PDF conformance checkers.

## Source Code and Licensing

This project's source code is availabe at [https://codeberg.org/thomasfischer-his/pdfaconfonline](https://codeberg.org/thomasfischer-his/pdfaconfonline) under the GNU General Public License version 3 or later (your choice).
Images used in this project were taken from various other places, their licenses are documented in the Git repository.
The used open source PDF validators, [veraPDF](https://verapdf.org/) and [Apache PDFBox Preflight](https://pdfbox.apache.org/) are available at their own webpages.

## The Service

There are two components to this service:

1. The script `monitor.sh` which continouously monitors a pre-configured 'watch' directory for appearing PDF files (as due to the upload script to be explained below) and then, for each PDF file, after some preliminary checks runs both veraPDF and Apache PDFBox on those. Both validation tools' outcomes are analyzed and a short report will be written into the pre-configured report directory.
1. A configuration for a lighttpd web server including the web server's configuration file `lighttpd.conf` and two CGI scripts that do the actual load:
    1. `index.sh` generates the main HTML file where the state of PDF files can be viewed: which files got uploaded and got queued for validation, which file is currently being validated, and what is the result for past, recent validations. Information for this page are drawn from the location of PDF files in various directories (see below) and existing report files. There is also the option to upload a PDF files making use of the other CGI script, `upload.py`.
    1. `upload.py` is a script which allows uploads of a PDF file from a HTML form such as employed by `index.sh`. The uploaded data will be preliminarily checked for validity and being a PDF file. Successfully uploaded PDF files will be placed into the 'watch' directory as checked by `monitor.sh`.

Both components are run separately and the only (asynchronous) communication that happens is through the appearance and disappearance of PDF files and report files.

A PDF file, while being processed, will be passed around through several directories:

1. While being uploaded, the growing file is placed in the 'upload' directory.
1. Once the upload is complete, the script `upload.py` will move the PDF file into the 'watch' directory.
1. Script `monitor.sh` will monitor 'watch' for any new PDF files. If there is a PDF file, it will pick one, move it to the 'processing' directory.
1. The single PDF file being in the 'processing' directory will be analyzed by both veraPDF and PDFBox.
1. Finally, the PDF file gets deleted and a PDF file-specific report text file will be written into the 'results' directory.

## Configuration

This project's configuration, such as which paths to be used to store PDF files or log data, is spread across several files.
Luckily, configuration settings are generalized with placeholders that can be customized to a user's needs.
To replace placeholders, run the following command (exemplified for variable `__BASEDIR__`):

```
sed -i -e 's!__BASEDIR__!/path/to/be/inserted!g' lighttpd.conf www/index.sh www/index.sh www/upload.py monitor.sh
```

The following variables are in use:

Variable | Description
---------|---------------
`__JAVA_HOME__` | Path were a suitable Java runtime environment is installed (used by veraPDF and PDFBox)
`__VERAPDFBINARY__` | Full and canonical paths to the veraPDF binary
`__PREFLIGHT_APP_JAR__` | Full and canonical paths to the Apache PDFBox Preflight app jar file
`__BASEDIR__` | Base directory where this repository's checkout resides
`__LOGDIR__`| Directory where logfiles, e.g. from lighttpd, are written to
`__PIDDIR__`| Directory where PID files (process identifiers), e.g. from lighttpd, are written to
`__UPLOADDIR__`| Directory where PDF files are stored which are under transfer
`__WATCHDIR__`| Directory where PDF files are queued after upload, waiting to be validated
`__PROCESSINGDIR__` | Directory where the PDF files to be analyzed by veraPDF and PDFBox are stored
`__RESULTSDIR__` | Directory where the result reports are stored

In `lighttpd.conf`, also check the paths to the Bash and the Python3 interpreter.

## Starting the Service

Before starting the service, ensure that the directories specified by the variables `__UPLOADDIR__` and `__LOGDIR__` exist and are writable by the user to which the lighttpd web server will belong.

To start the service, the two components as detailed above need to run independently:

1. The lighttpd web server can be launched like any other lighttpd instance, given that the configuration file `lighttpd.conf` is properly adjusted and passed to the web server instance for use.
1. At the same time, the Bash script `monitor.sh` must be running in order to watch the pre-configured directory for incoming PDF files, invoking the validators, and eventually creating reports.
