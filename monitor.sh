#!/usr/bin/env bash

# SPDX-FileCopyrightText: Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: GPL-3.0-or-later

export JAVA_HOME="__JAVA_HOME__"

WATCH_DIRECTORY="__WATCHDIR__"
CONFORMANCECHECKER_WORKING_DIRECTORY="__PROCESSINGDIR__"
RESULT_DIRECTORY="__RESULTSDIR__"
mkdir -p "${WATCH_DIRECTORY}" "${CONFORMANCECHECKER_WORKING_DIRECTORY}" "${RESULT_DIRECTORY}"

VERAPDF_BINARY="__VERAPDFBINARY__"
PREFLIGHT_APP_JAR="__PREFLIGHT_APP_JAR__"

[[ -s "${VERAPDF_BINARY}" && -n "${VERAPDF_BINARY}" && -x "${VERAPDF_BINARY}" ]] || { echo "Not a valid veraPDF binary: ${VERAPDF_BINARY}" >&2 ; exit 1 ; }
[[ -s "${PREFLIGHT_APP_JAR}" && -n "${PREFLIGHT_APP_JAR}" ]] || { echo "Not a valid PDFBox .jar file: ${PREFLIGHT_APP_JAR}" >&2 ; exit 1 ; }

function processpdffile() {
	local pdffilename="$1"

	test -s "${pdffilename}" || { echo "Non-existing or empty file: ${pdffilename}" >&2 ; rm -f "${pdffilename}" ; return 1 ; }
	file "${pdffilename}" | grep -q ': PDF document, ' || { echo "Not a PDF file: ${pdffilename}" >&2 ; rm -f "${pdffilename}" ; return 1 ; }

	rm -rf "${CONFORMANCECHECKER_WORKING_DIRECTORY}" || { rm -f "${pdffilename}" ; return 1 ; }
	mkdir -p "${CONFORMANCECHECKER_WORKING_DIRECTORY}" || { rm -f "${pdffilename}" ; return 1 ; }
	
	mv "${pdffilename}" "${CONFORMANCECHECKER_WORKING_DIRECTORY}/$(basename "${pdffilename/.PDF/.pdf}")" || { rm -f "${pdffilename}" ; return 1 ; }
	pdffilename="$(ls -1 "${CONFORMANCECHECKER_WORKING_DIRECTORY}/$(basename "${pdffilename/.PDF/.pdf}")")"
	test -n "${pdffilename}" || return 1
	test -s "${pdffilename}" || { rm -f "${pdffilename}" ; return 1 ; }

	verapdfreportfilename="$(sed -e 's!.pdf$!-verapdf-report.txt!i' <<<"${pdffilename}")"
	[[ "${verapdfreportfilename}" != "${pdffilename}" ]] || { echo "Filename doesn't seem to end in '.pdf': ${pdffilename}" >&2 ; rm -f "${pdffilename}" ; return 1 ; }
	preflightreportfilename="$(sed -e 's!.pdf$!-preflight-report.txt!i' <<<"${pdffilename}")"
	[[ "${preflightreportfilename}" != "${pdffilename}" ]] || { echo "Filename doesn't seem to end in '.pdf': ${pdffilename}" >&2 ; rm -f "${pdffilename}" ; return 1 ; }

	echo "Running veraPDF on '${pdffilename}'" >&2
	nice -n 15 ionice -c 3 "${VERAPDF_BINARY}" "${pdffilename}" &>"${verapdfreportfilename}" &
	
	echo "Apache Preflight on '${pdffilename}'" >&2
	nice -n 15 ionice -c 3 java -jar "${PREFLIGHT_APP_JAR}" "${pdffilename}" &>"${preflightreportfilename}" &

	wait

	verapdfsummaryfilename="$(sed -e 's!.pdf$!-verapdf-summary.txt!i' <<<"${pdffilename}")"
	grep --color=NEVER -E 'failedToParse=|<validationReport profileName=' <"${verapdfreportfilename}" >"${verapdfsummaryfilename}" || { echo "Failed to identify result in veraPDF report" >&2 ; head -n 60 "${verapdfreportfilename}" >&2 ; rm -f "${pdffilename}" "${verapdfreportfilename}" "${preflightreportfilename}" "${verapdfsummaryfilename}" ; return 1 ; }

	preflightsummaryfilename="$(sed -e 's!.pdf$!-pdfbox-summary.txt!i' <<<"${pdffilename}")"
	head -n 60 "${preflightreportfilename}" >&2
	strings <"${preflightreportfilename}" | grep --color=NEVER '^The file .* PDF/A-[1-3][ABUabu] file' >"${preflightsummaryfilename}" || { echo "Failed to identify result in Apache Preflight report" >&2 ; head -n 60 "${preflightreportfilename}" >&2 ; rm -f "${pdffilename}" "${verapdfreportfilename}" "${preflightreportfilename}" "${verapdfsummaryfilename}" "${preflightsummaryfilename}" ; return 1 ; }

	if grep -q 'batchSummary totalJobs="1" failedToParse="1"' "${verapdfsummaryfilename}" ; then
		verapdfiscompliant=false
		verapdfvalidationprofile="Error"
	else
		verapdfiscompliant="$(grep -Po --color=NEVER 'isCompliant="(true|false)"' <"${verapdfsummaryfilename}" | head -n1 | sed -e 's!"!!g;s!isCompliant=!!g')"
		verapdfvalidationprofile="$(grep -Po --color=NEVER '"PDF/A-[1-3][ABUabu] validation profile"' <"${verapdfsummaryfilename}" | head -n1 | sed -e 's!"!!g;s! validation profile!!g;s!A$!a!;s!B$!b!;s!U$!u!')"
	fi
	preflightvalidationprofile="$(grep -Po --color=NEVER ' PDF/A-[1-3][ABUabu] ' <"${preflightsummaryfilename}" | head -n1 | sed -e 's! !!g')"
	preflightiscompliant="$(grep -Po --color=NEVER ' is (not )?a valid PDF' <"${preflightsummaryfilename}" | head -n1 | sed -e 's! is not a valid PDF!false!g;s! is a valid PDF!true!g')"
	resultfilename="${RESULT_DIRECTORY}/$(sed -e 's!.pdf$!-result.txt!i' <<<"$(basename "${pdffilename}")")"
	echo "VERAPDF:${verapdfvalidationprofile}:${verapdfiscompliant}" >"${resultfilename}"
	echo "PDFBOXPREFLIGHT:${preflightvalidationprofile}:${preflightiscompliant}" >>"${resultfilename}"
	echo "FILECREATIONDATE:$(ls -l --block-size=1 --time-style='+%s' "${pdffilename}" | grep -Po --color=NEVER '\b(1[5-9]|[23]\d)\d{8}\b' | head -n1)" >>"${resultfilename}"
	echo "REPORTDATE:$(ls -l --block-size=1 --time-style='+%s' "${resultfilename}" | grep -Po --color=NEVER '\b(1[5-9]|[23]\d)\d{8}\b' | head -n1)" >>"${resultfilename}"
	echo "ORIGINALFILENAME:$(basename "${pdffilename}")" >>"${resultfilename}"

	rm -f "${pdffilename}" "${verapdfreportfilename}" ${preflightreportfilename} "${verapdfsummaryfilename}" "${preflightsummaryfilename}"
	return 0
}

while /bin/true ; do
	stillwatching=1
	while (( ${stillwatching} > 0 )) ; do
		stillwatching=0
		while read pdffilename ; do
			[[ "${pdffilename}" = '*.pdf' ]] && break
			test -s "${pdffilename}" || break
			processpdffile "${pdffilename}"
			(( ++stillwatching ))
		done <<<$(find "${WATCH_DIRECTORY}" -mindepth 1 -maxdepth 1 -type f -iname '*.pdf')
	done
	inotifywait -e moved_to,create "${WATCH_DIRECTORY}" || break
done
echo "Stopped continuous monitoring" >&2
