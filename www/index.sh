# SPDX-FileCopyrightText: Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: GPL-3.0-or-later

export LANG=
export LC_ALL=C

echo '<!DOCTYPE html>'
echo '<html><head><meta charset="UTF-8"><meta name="author" content="Thomas Fischer"><title>Online PDF/A Conformance Checker</title><link type="text/css" rel="stylesheet" href="style.css"></head>'
echo '<body>'

echo '<h1>Online PDF/A Conformance Checker</h1>'

echo '<div class="specialblock">'
echo '<p>Security warning: Do not upload PDF files that contain sensitive, private, or classified information. This service does not guarantee the confidentiality of any data submitted to it.</p>'
echo '<p><form enctype="multipart/form-data" action="upload.py" method="post"><label for="pdffile">PDF file to check:</label> <input id="pdffile" type="file" name="pdffile"> <input type="submit" value="Upload"></form></p>'
echo '</div>'

echo '<div class="specialblock">'
echo '<p><label for="refreshbutton">Click to update the tables blow:</label> <button id="refreshbutton" onClick="window.location.href=window.location.href">Refresh Page</button></p>'
echo '</div>'


echo '<div class="block">'
echo '<h2>Queued PDF Files</h2>'
countresults=$(ls -1 "__WATCHDIR__"/*.pdf 2>/dev/null | wc -l)
if (( ${countresults} > 0 )) ; then
	echo '<table class="queue"><thead><tr><th class="filename">Filename</th><th class="uploaddate">Upload date/time</th></tr></thead><tbody>'
	ls -1t "__WATCHDIR__"/*.pdf 2>/dev/null | head -n 16 | while read pdffilename ; do
		echo -n '<tr><td class="filename"><img src="waiting.webp" width="22" height="22" style="padding-right:0.5ex;" alt="Waiting to be processed"/>'
		basename "${pdffilename}" | sed -e 's!>!&gt;!g;s!<!&lt;!g'
		echo '</td><td class="uploaddate">'
		TZ="Europe/Stockholm" date --date="@$(ls -l --block-size=1 --time-style='+%s' "${pdffilename}" | grep -Po --color=NEVER '\b(1[5-9]|[23]\d)\d{8}\b' | head -n1)" '+%Y-%m-%d, %T'
		echo '</td></tr>'
	done
	echo '</tbody></table>'
else
	echo '<p class="noresults">No files</p>'
fi
echo '</div>'


echo '<div class="block">'
echo '<h2>PDF Files Being Processed</h2>'
countresults=$(ls -1 "__PROCESSINGDIR__"/*.pdf 2>/dev/null | wc -l)
if (( ${countresults} > 0 )) ; then
	echo '<table class="processed"><thead><tr><th class="filename">Filename</th><th class="uploaddate">Upload date/time</th></tr></thead><tbody>'
	ls -1t "__PROCESSINGDIR__"/*.pdf 2>/dev/null | head -n 16 | while read pdffilename ; do
		echo -n '<tr><td class="filename"><img src="busy.gif" width="16" height="16" style="padding-right:0.5ex;" alt="Currently being processed"/>'
		basename "${pdffilename}" | sed -e 's!>!&gt;!g;s!<!&lt;!g'
		echo '</td><td class="uploaddate">'
		TZ="Europe/Stockholm" date --date="@$(ls -l --block-size=1 --time-style='+%s' "${pdffilename}" | grep -Po --color=NEVER '\b(1[5-9]|[23]\d)\d{8}\b' | head -n1)" '+%Y-%m-%d, %T'
		echo '</td></tr>'
	done
	echo '</tbody></table>'
	echo '<p><label for="refreshbutton">No updates in this table? </label> <button id="refreshbutton" onClick="window.location.href=window.location.href">Refresh Page</button></p>'
else
	echo '<p class="noresults">No files</p>'
fi
echo '</div>'


echo '<div class="block">'
echo '<h2>Recently Checked PDF Files</h2>'
countresults=$(ls -1 "__RESULTSDIR__"/*-result.txt 2>/dev/null | wc -l)
if (( ${countresults} > 0 )) ; then
	echo '<table class="results"><thead><tr><th class="filename">Filename</th><th class="uploaddate">Upload date/time</th><th class="reportdate">Report date/time</th><th class="resultverapdf">veraPDF</th><th class="resultpdfbox">Apache PDFBox</th></tr></thead><tbody>'
	ls -1t "__RESULTSDIR__"/*-result.txt 2>/dev/null | head -n 16 | while read resultfilename ; do
		verapdfbooleanresult="$(grep -E '^VERAPDF:' "${resultfilename}" | cut -f 3 -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')"
		pdfboxbooleanresult="$(grep -E '^PDFBOXPREFLIGHT:' "${resultfilename}" | cut -f 3 -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')"
		verapdfprofileresult="$(grep -E '^VERAPDF:' "${resultfilename}" | cut -f 2 -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')"
		pdfboxprofileresult="$(grep -E '^PDFBOXPREFLIGHT:' "${resultfilename}" | cut -f 2 -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')"
		trafficlightcolor="red"
		if [[ "${verapdfbooleanresult}" = "true" && "${pdfboxbooleanresult}" = "true" ]] ; then
			if [[ "${verapdfprofileresult}" = PDF/A-1* && "${pdfboxprofileresult}" = PDF/A-1* ]] ; then
				trafficlightcolor="green"
				explanation="File got recognized as conforming to PDF/A-1 by both conformance checkers"
			else
				trafficlightcolor="yellow"
				explanation="File got recognized as conforming to PDF/A by both conformance checkers, but <em>not</em> to PDF/A-<strong>1</strong>"
			fi
		elif [[ "${verapdfbooleanresult}" != "true" && "${pdfboxbooleanresult}" != "true" ]] ; then
			trafficlightcolor="red"
			explanation="File failed all tests for PDF/A conformance"
		elif [[ "${verapdfbooleanresult}" = "true" || "${pdfboxbooleanresult}" = "true" ]] ; then
			if [[ ( "${verapdfprofileresult}" = PDF/A-1* && "${verapdfbooleanresult}" = "true" ) || ( "${pdfboxprofileresult}" = PDF/A-1* && "${pdfboxbooleanresult}" = "true" ) ]] ; then
				trafficlightcolor="yellow"
				explanation="File got recognized as conforming to PDF/A-1 by only one conformance checker"
			else
				trafficlightcolor="red"
				explanation="File got recognized as conforming to PDF/A by one conformance checker, but <em>not</em> to PDF/A-<strong>1</strong>"
			fi
		fi
		if [[ "${trafficlightcolor}" = "green" ]] ; then
			tdcolor="#00ff0033"
			trafficlight='<img src="trafficlight-green.webp" width="22" height="22" style="padding-right:0.5ex;" />'
		elif [[ "${trafficlightcolor}" = "yellow" ]] ; then
			tdcolor="#ffee0033"
			trafficlight='<img src="trafficlight-yellow.webp" width="22" height="22" style="padding-right:0.5ex;" />'
		else
			tdcolor="#ff000033"
			trafficlight='<img src="trafficlight-red.webp" width="22" height="22" style="padding-right:0.5ex;" />'
		fi
		echo -n '<tr><td style="background: '"${tdcolor}"';" class="filename tooltip">'"${trafficlight}"
		grep -E '^ORIGINALFILENAME:' "${resultfilename}" | cut -f 2- -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g'
		echo -n '<span class="tooltiptext">&#8592; '"${explanation}"'</span>'
		echo '</td><td class="uploaddate">'
		TZ="Europe/Stockholm" date --date="@$(grep -E 'FILECREATIONDATE' "${resultfilename}" | cut -f 2- -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')" '+%Y-%m-%d, %T'
		echo '</td><td class="reportdate">'
		TZ="Europe/Stockholm" date --date="@$(grep -E 'REPORTDATE' "${resultfilename}" | cut -f 2- -d ':' | sed -e 's!>!&gt;!g;s!<!&lt;!g')" '+%Y-%m-%d, %T'
		echo '</td><td class="resultverapdf">'
		if [ "${verapdfbooleanresult}" = "true" ] ; then
			echo "${verapdfprofileresult}"'<img src="opinion-okay.webp" height="22" width="22" style="padding-left:0.5ex;" alt="Passed" />'
		elif [ "${verapdfbooleanresult}" = "false" ] ; then
			echo "${verapdfprofileresult}"'<img src="opinion-no.webp" height="22" width="22" style="padding-left:0.5ex;" alt="Passed" />'
		fi
		echo '</td><td class="resultpdfbox">'
		if [ "${pdfboxbooleanresult}" = "true" ] ; then
			echo "${pdfboxprofileresult}"'<img src="opinion-okay.webp" height="22" width="22" style="padding-left:0.5ex;" alt="Passed" />'
		elif [ "${pdfboxbooleanresult}" = "false" ] ; then
			echo "${pdfboxprofileresult}"'<img src="opinion-no.webp" height="22" width="22" style="padding-left:0.5ex;" alt="Passed" />'
		fi
		echo '</td></tr>'
	done
	echo '</tbody></table>'
else
	echo '<p class="noresults">No results to show</p>'
fi
echo '</div>'

echo '<p style="font-size:90%; color:rgba(0,0,0,0.6667);">This project&apos;s source code is availabe at <a style="color:rgba(0,0,0,0.75);" href="https://codeberg.org/thomasfischer-his/pdfaconfonline" target="_top">https://codeberg.org/thomasfischer-his/pdfaconfonline</a> under the <a style="color:rgba(0,0,0,0.75);" href="https://www.fsf.org/licensing/licenses/gpl-3.0.html" target="_top">GNU General Public License version 3</a> or later (your choice). Images used on this webpage were taken from various other projects and places, their licenses are documented in the Git repository. The used open source PDF validators, <a style="color:rgba(0,0,0,0.75);" href="https://verapdf.org/" target="_top" style>veraPDF</a> and <a style="color:rgba(0,0,0,0.75);" href="https://pdfbox.apache.org/" target="_top">Apache PDFBox Preflight</a> are available at their own webpages.</p>'

echo '<p style="font-size:90%; color:rgba(0,0,0,0.6667);">This service does not make use of cookies. However, it will log its users&apos; activities, such as date/time and origin of access for any relevant activity such as page (re)loads and file uploads. Uploaded PDF files and any data actively submitted to this service may get stored for an undetermined duration of time.</p>'

echo '<p>This page was generated at:'
LC_TIME= LC_ALL= LANG= LC_DATE= date --utc
echo '</p>'

echo '</body></html>'
