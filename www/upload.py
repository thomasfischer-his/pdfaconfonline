# SPDX-FileCopyrightText: Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: GPL-3.0-or-later

import cgi, os,sys,codecs,re
import cgitb
import binascii
cgitb.enable()

sys.stderr = sys.stdout

print("Content-Type: text/html; charset=utf-8")
print()
message = ""

try:
	form = cgi.FieldStorage()
	pdffileitem = form['pdffile']

	cleaned_filename=None
	if pdffileitem.filename:
		cleaned_filename=pdffileitem.filename.encode('utf-8').replace(b'\xc3\xa4',b'a').replace(b'\xc3\xa5',b'a').replace(b'\xc3\xa9',b'e').replace(b'\xc3\xb6',b'o').replace(b'\xc3\x84',b'A').replace(b'\xc3\x85',b'A').replace(b'\xc3\x89',b'e').replace(b'\xc3\x96',b'O').decode('ascii', 'replace').replace(u'\ufffd', 'X')
		if len(cleaned_filename)<5:
			message = "Filename is too short"
			cleaned_filename=None
		else:
			if "\\" in cleaned_filename or ".." in cleaned_filename or "/" in cleaned_filename or "\x00" in cleaned_filename or "\x0a" in cleaned_filename or "\x09" in cleaned_filename or "\x0d" in cleaned_filename:
				message = "Filename contains invalid characters"
				cleaned_filename=None
			else:
				if len(cleaned_filename)>64:
					cleaned_filename=cleaned_filename[:60]+cleaned_filename[-4:].lower()
				else:
					cleaned_filename=cleaned_filename[:-4]+cleaned_filename[-4:].lower()
				
				if cleaned_filename[-4:]!=".pdf":
					message = "Filename does not end with '.pdf'"
					cleaned_filename=None
				else:
					firstbytes=pdffileitem.file.read(8)
					if len(firstbytes)<8 or chr(firstbytes[0])!='%' or chr(firstbytes[1])!='P' or chr(firstbytes[2])!='D' or chr(firstbytes[3])!='F' or chr(firstbytes[4])!='-' or (chr(firstbytes[5])!='1' and chr(firstbytes[5])!='2') or chr(firstbytes[6])!='.' or chr(firstbytes[7])<'0' or chr(firstbytes[7])>'9':
						message = "First bytes of file do not contain PDF header"
						cleaned_filename=None
	else:
		message = "No 'pdffileitem.filename' not given"

	success=cleaned_filename and len(cleaned_filename)>=5

	if success:
		with open("__UPLOADDIR__/"+cleaned_filename,"wb") as pdffile:
			pdffile.write(firstbytes)
			pdffile.write(pdffileitem.file.read(1<<22))
		os.replace("__UPLOADDIR__/"+cleaned_filename,"__WATCHDIR__/"+cleaned_filename)

except Exception as e:
	message = "Exception:"+str(e)
	success=False

print('<!DOCTYPE html>')
print("<html><head><meta charset=\"UTF-8\"><title>Upload "+("succeded" if success else "failed")+"</title></head>")
if success:
	print("<body onload='window.location.replace(\"/\");'>")
	print("<h1>Upload succeded</h1><p>Filename:",cleaned_filename,"</p><p><a href=\"/\" target=\"_top\">Return to main page</a></p>")
else:
	print("<body>")
	print("<h1>Upload failed</h1><p>Possible reasons include:</p><ul><li>You pressed accidentially on &lsquo;Upload&rsquo; without specifying a filename</li><li>The filename contained characters that were not acceptable. Stick to ASCII characters plus &aring;, &auml;, and &ouml;.</li><li>The uploaded file was not a PDF file.</li><li>There is a technical problem which requires <a href=\"mailto:thomas.fischer@his.se\">assistance</a>.</a></ul><p><a href=\"/\" target=\"_top\">Return to main page</a></p>")
	print("<p>The following error message may help:</p><pre>"+message+"</pre>")
print("<p style=\"font-size:90%; color:rgba(0,0,0,0.6667);\">This project&apos;s source code is availabe at <a style=\"color:rgba(0,0,0,0.75);\" href=\"https://codeberg.org/thomasfischer-his/pdfaconfonline\" target=\"_top\">https://codeberg.org/thomasfischer-his/pdfaconfonline</a> under the <a style=\"color:rgba(0,0,0,0.75);\" href=\"https://www.fsf.org/licensing/licenses/gpl-3.0.html\" target=\"_top\">GNU General Public License version 3</a> or later (your choice). Images used on this webpage were taken from various other projects and places, their licenses are documented in the Git repository. The used open source PDF validators, <a style=\"color:rgba(0,0,0,0.75);\" href=\"https://verapdf.org/\" target=\"_top\" style>veraPDF</a> and <a style=\"color:rgba(0,0,0,0.75);\" href=\"https://pdfbox.apache.org/\" target=\"_top\">Apache PDFBox Preflight</a> are available at their own webpages.</p>")
print("</body></html>")
